#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include <ncurses.h>
#include "draw.hpp"
#include "player.hpp"
#include "map.hpp"


using namespace std;
Draw::Draw(){}

void Draw::printGamestart(){
  printw("*******************||BEM VINDO||*******************\n");
  printw("******||APERTE UMA TECLA PARA INICIAR O JOGO||*****\n");

}

void Draw::printScore(Player *player,Map *map){
  map->printMap();
  printw("    **************                \n");
  printw("    * Vidas: %d   *                \n",player->getLifes());
  printw("    * Pontos: %d *                \n",player->getScore());
  printw("    **************                \n");
}


void Draw::printMap(){
	for(int i = 0; i < 10; i++){
		for(int u = 0; u < 80; u++){
    	printw("%c", this->originalMap[i][u]);
      if(u >= 79){
      	printw("\n");
			}
    }
  }
}

void Draw::printWin(){
	for(int i = 0; i < 20; i++){
		for(int u = 0; u < 174; u++){
    	printw("%c", this->originalMap[i][u]);
      if(u >= 173){
      	printw("\n");
			}
    }
  }
}
