#include <iostream>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "map.hpp"

using namespace std;

Map::Map(){}

Map::~Map(){}

void Map::catchMap(){
	ifstream infile("map.txt");
	string aux;

	for(int contLinha = 0; contLinha < 20; contLinha++){
		getline(infile,aux);
		for(int contColuna = 0; contColuna < 50; contColuna++){
			this->range[contLinha][contColuna] = aux[contColuna];
			this->newRange[contLinha][contColuna] = aux[contColuna];
		}
	}
	infile.close();
}

void Map::printMap(){
	for(int contLinha = 0; contLinha < 20; contLinha++){
		for(int contColuna = 0; contColuna < 50; contColuna++){
    	printw("%c", this->range[contLinha][contColuna]);
      this->range[contLinha][contColuna] = this->newRange[contLinha][contColuna];
      if(contColuna >= 49){
      	printw("\n");
			}
    }
  }
}

char Map::catchPositionRange(int positionY, int positionX){
	return this->range[positionY][positionX];
}

char Map::catchPositionNewRange(int positionY, int positionX){
	return this->newRange[positionY][positionX];
}

void Map::addElementRange(char sprite, int positionY, int positionX){
	this->range[positionY][positionX] = sprite;
}

void Map::addElementNewRange(char sprite,int positionY, int positionX){
	this->newRange[positionY][positionX] = sprite;
}
