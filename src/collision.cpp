#include <iostream>
#include <stdio.h>
#include <string>
#include <ncurses.h>
#include "collision.hpp"
#include "trap.hpp"
#include "gameObject.hpp"
#include "bonus.hpp"
#include "map.hpp"
#include "player.hpp"

using namespace std;

Collision::Collision(){}

bool Collision::getIdentifyCollision(){
  return identifyCollision;
}

void Collision::setIdentifyCollision(bool identifyCollision){
  this->identifyCollision = identifyCollision;
}

void Collision::collide(Player *player, Map *map){
  Map *character;
  char direction;
  direction = getch();

  if(direction == 'w' && character->catchPositionRange(player->getPositionY()-1, player->getPositionX()) != '='){
    player->setPositionY(player->getPositionY()-1);
  }

  else if(direction == 's' && character->catchPositionRange(player->getPositionY()+1, player->getPositionX()) != '='){
    player->setPositionY(player->getPositionY()+1);
  }

  else if(direction == 'a' && character->catchPositionRange(player->getPositionY(), player->getPositionX()-1) != '='){
    player->setPositionX(player->getPositionX()-1);
  }

  else if(direction == 'd' && character->catchPositionRange(player->getPositionY(), player->getPositionX()+1) != '='){
    player->setPositionX(player->getPositionX()+1);
  }
}

void Collision::collideBonus(Player *player,Map *map){
  if(map->catchPositionRange(player->getPositionY(),player->getPositionX()) == 'o' ){
    player->setScore(player->getScore()+10);
    map->addElementNewRange('-',player->getPositionY(),player->getPositionX());
  }

  else if(map->catchPositionRange(player->getPositionY(),player->getPositionX()) == 'x' ){
    player->setLifes(player->getLifes()-1);
    map->addElementNewRange('-',player->getPositionY(),player->getPositionX());
  }
}

void Collision::endGame(Player * player, Map * map){
	char character;
	character = map->catchPositionRange((player->getPositionY()),(player->getPositionX()));

	if(character == '8'){
		player->setWinner(TRUE);
	}
}
