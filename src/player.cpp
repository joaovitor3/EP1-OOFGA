#include"player.hpp"
#include<iostream>
#include<ncurses.h>

using namespace std;

Player::Player(){
	setSprite('@');
	setPositionX(3);
	setPositionY(3);
	setAlive(1);
	setWinner(FALSE);
	setScore(0);
	setLifes(3);
}

Player::Player(int positionX, int positionY){
    setSprite('@');
    setPositionX(positionX);
    setPositionY(positionY);
}

Player::~Player(){}

void Player::setAlive(bool alive){
    this->alive = alive;
}
bool Player::getAlive(){
    return this->alive;
}

void Player::setScore(int score){
    this->score = score;
}
int Player::getScore(){
    return score;
}

void Player::setLifes(int lifes){
    this->lifes = lifes;
}
int Player::getLifes(){
    return lifes;
}

void Player::setWinner(bool winner){
    this->winner = winner;
}
bool Player::getWinner(){
    return winner;
}
