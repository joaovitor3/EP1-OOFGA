#include <ncurses.h>
#include <iostream>
#include <cstdlib>
#include "gameObject.hpp"
#include "map.hpp"
#include "collision.hpp"
#include "bonus.hpp"
#include "trap.hpp"
#include "draw.hpp"
#include "player.hpp"

using namespace std;

int main(){
  Map * map = new Map();
  Player * player = new Player();
  Draw * draw = new Draw();
  map -> catchMap();

  Trap * trap[8];
	Bonus * bonus[8];

  int i[6],d[6],c,cont=0,by[3],hx[3],contador = 0;

  Collision * collide = new Collision();

  for(c = 0;c < 8; c++){
		d[c] = 1+(rand() % 19);
		i[c] = 1+(rand() % 49);
		trap[c] = new Trap();
		by[cont] = 1+(rand() % 19);
		hx[cont] = 1+(rand() % 49);
		bonus[cont] = new Bonus();
		cont++;
	}

  for(c = 0;c < 8; c++){
		map->addElementNewRange(trap[c]->getSprite(),d[c],i[c]);
	}
	for(cont = 0; cont < 8; cont++){
		map->addElementNewRange(bonus[cont]->getSprite(),by[cont],hx[cont]);
	}
  char stop;
  while(!player->getWinner() && !(player->getLifes() == 0)){
  	initscr();
    clear();
  	if(contador == 0){
  		draw->printGamestart();
  	}
    map->addElementRange(player->getSprite(),player->getPositionY(),player->getPositionX());
    draw->printScore(player,map);
  	collide->collide(player,map);
  	collide->collideBonus(player,map);
  	collide->endGame(player,map);
    contador++;
    if(player->getWinner() == TRUE){
    initscr();
    clear();
    printw("PARABENS!");
    printw("VOCE GANHOU!");
    stop = getch();
    refresh();
    endwin();
    }
  }

  clear();
  initscr();
  if(!(player->getWinner())){
    printw("VOCE PERDEU !!\n");
    printw(" = ( \n");
    stop = getch();
    refresh();
    endwin();
  }

  return 0;
}
