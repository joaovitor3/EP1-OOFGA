#include"gameObject.hpp"
#include<ncurses.h>
#include<iostream>

using namespace std;

GameObject::GameObject(){
  setPositionX(40);
  setPositionY(40);
  setSprite('p');
}

GameObject::~GameObject(){}

void GameObject::setPositionX(int positionX){
    this->positionX = positionX;
}
int GameObject::getPositionX(){
    return positionX;
}

void GameObject::setPositionY(int positionY){
    this->positionY = positionY;
}
int GameObject::getPositionY(){
    return positionY;
}

void GameObject::setSprite(char sprite){
    this->sprite = sprite;
}
char GameObject::getSprite(){
    return sprite;
}
