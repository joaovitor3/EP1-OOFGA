#include"bonus.hpp"
#include<iostream>
#include<ncurses.h>
#include"gameObject.hpp"

using namespace std;

Bonus::Bonus(){
  setSprite('o');
}

Bonus::Bonus(int positionX, int positionY, char sprite){
    setSprite(sprite);
    setPositionX(positionX);
    setSprite(sprite);
}

void Bonus::setScore(int score){
    this->score = score;
}
int Bonus::getScore(){
    return score;
}
