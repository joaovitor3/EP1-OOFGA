#include"trap.hpp"
#include<iostream>
#include<ncurses.h>

using namespace std;

Trap::Trap(){
  setSprite('x');
}

Trap::Trap(char sprite, int positionX, int positionY){
    setPositionX(positionX);
    setPositionY(positionY);
    setSprite(sprite);
}

void Trap::setDamage(int damage){
    this->damage = damage;
}
int Trap::getDamage(){
    return damage;
}
