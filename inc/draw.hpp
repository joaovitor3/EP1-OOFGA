#ifndef DRAW_HPP
#define DRAW_HPP

#include <iostream>
#include <stdio.h>
#include <string>
#include <ncurses.h>
#include "map.hpp"
#include "player.hpp"

class Draw{
private:
    //attributes
    int originalMap[20][50];
    int newMap[20][50];
public:
    //methods
    Draw();
    void printGamestart();
    void printScore(Player *player,Map *map);
    void printMap();
    void printWin();
};

#endif
