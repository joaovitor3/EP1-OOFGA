#ifndef COLLISION_HPP
#define COLLISION_HPP

#include <iostream>
#include <stdio.h>
#include <string>
#include <ncurses.h>
#include "trap.hpp"
#include "gameObject.hpp"
#include "bonus.hpp"
#include "map.hpp"
#include "player.hpp"

class Collision{
private:
    bool identifyCollision=TRUE;

public:
		Collision();
    bool getIdentifyCollision();
    void setIdentifyCollision(bool identifyCollision);

    void collide(Player * player, Map *map);
    void collideBonus(Player * player, Map * map);
    void endGame(Player * player, Map * map);
};

#endif
