#ifndef PLAYER_HPP
#define PLAYER_HPP

#include"gameObject.hpp"
#include<string>

class Player : public GameObject{
  private:
    //attributes
    bool alive;
    int score;
    int lifes;
    bool winner;
  public:
    //methods
    Player();
    Player(int positionX, int positionY);
    ~Player();
    void setAlive(bool alive);
    bool getAlive();
    void setScore(int score);
    int getScore();
    void setLifes(int lifes);
    int getLifes();
    void setWinner(bool winner);
    bool getWinner();
};

#endif
