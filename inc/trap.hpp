#ifndef TRAP_HPP
#define TRAP_HPP

#include "gameObject.hpp"
#include <string>

class Trap : public GameObject{
  private:
    //atributes
    int damage;
  public:
    //methods
    Trap();
    ~Trap();
    Trap(char sprite, int positionX, int positionY);
    void setDamage(int damage);
    int getDamage();

};

#endif
