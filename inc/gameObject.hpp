#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include<string>

class GameObject{
  private:
    //attributes
    int positionX;
    int positionY;
    char sprite;

  public:
    //methods
    GameObject(); //constructor
    ~GameObject();
    void setPositionX(int positionX);  //accessor set
    int getPositionX();  //accessor get
    void setPositionY(int positionY);
    int getPositionY();
    void setSprite(char sprite);
    char getSprite();

};

#endif
