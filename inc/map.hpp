#ifndef MAP_HPP
#define MAP_HPP

#include <iostream>
#include <string>

class Map{
private:
		char range[20][50];
		char newRange[20][50];

public:
		Map();
		~Map();
		void catchMap();
		void printMap();
		char catchPositionRange(int positionY, int positionX);
		char catchPositionNewRange(int positionY, int positionX);
		void addElementRange(char sprite,int positionY,int positionX);
		void addElementNewRange(char sprite,int positionY,int positionX);

};

#endif
