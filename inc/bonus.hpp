#ifndef BONUS_HPP
#define BONUS_HPP

#include "gameObject.hpp"
#include <string>

class Bonus : public GameObject{
  private:
    //atributes
    int score;
  public:
    //methods
    Bonus();
    ~Bonus();
    Bonus(int positionX, int positionY, char sprite);
    void setScore(int score);
    int getScore();

};

#endif
