Exercício de programação 1
Trabalho feito para disciplina de OO 2017/1
Esse documento serve para a descrição de como ocorre o funcionamento do jogo.
O jogo foi desenvolvido nos sistemas unix, nesse caso específico em uma distribuição do ubuntu.
Para o correto funcionamento do jogo é necessária a instalação do compilador gcc, make e da biblioteca ncurses.h para leituras de determinadas partes do programa.
Após esses instalados o jogo poderá exercer sua função corretamente.
Para a correta compilação do jogo  deve ser feita a devida organização de pastas:

-Pasta do projeto(nesse caso EP1-OOFGA) que engloba todas os seguintes:
->Pasta bin: Utilizada para se guardar o arquivo executável (.o)
->Pasta inc: Utilizada para guardar os arquivos .hpp
->Pasta src: Utilizada para guardar os arquivos .src
->Pasta doc: Utilizada para guardar os arquivos .txt
->Makefile: utilizado para a compilação dos arquivos
->map.txt: utilzado para a importação do mapa de jogo

Após essa organização e downloads dos arquivos ditos acima deve-se entrar na pasta do projeto pelo terminal e digitar o comando make para compilação, após isso deve-se digitar o comando make run para se rodar o programa.
Em seguida o jogo será aberto.
O objetivo do jogo é atravessar o labirinto, coletar o máximo de bônus (que geram pontos) e chegar ao caractere '8' que significa o fim do labirinto.
O Caractere '@' corresponde ao jogador, o caractere '=' corresponde a uma parede e não deve ser atravessada pelo jogador, o caractere 'x' corresponde a armadilha e quando o jogador passar por ele deve perder uma vida (ao perder três vidas o jogador perde o jogo) . O caractere 'o' corresponde ao bônus e é incrementado 10 pontos a cada bônus coletado.


